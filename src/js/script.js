const dialogs = {
	newTeacher: document.getElementById('newTeacher'),
	infocardTeacher: document.getElementById('infocardTeacher'),
};

const buttons = {
	addTeacher: document.querySelectorAll('.add__button'),
	cancelDialog: document.querySelectorAll('.cancel'),
	teachersItems: document.querySelectorAll('.teachers__item'),
};

function ActivatingModalWindow(dialog) {
	const dia = dialog;
	dia.style.display = 'flex';
	dia.setAttribute('open', 'open');
	document.querySelector('body').classList.add('_dialog');
}

function DeactivatiONModalWindow() {
	dialogs.newTeacher.style.display = 'none';
	dialogs.newTeacher.removeAttribute('open');
	dialogs.infocardTeacher.style.display = 'none';
	dialogs.infocardTeacher.removeAttribute('open');
	document.querySelector('body').classList.remove('_dialog');
}

for (let i = 0; i < buttons.addTeacher.length; i++) {
	buttons.addTeacher[i].onclick = () => ActivatingModalWindow(dialogs.newTeacher);
}

for (let i = 0; i < buttons.cancelDialog.length; i++) {
	buttons.cancelDialog[i].onclick = () => DeactivatiONModalWindow();
}

for (let i = 0; i < buttons.teachersItems.length; i++) {
	buttons.teachersItems[i].onclick = () => ActivatingModalWindow(dialogs.infocardTeacher);
}
