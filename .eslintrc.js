module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2021: true,
	},
	extends: [
		'airbnb-base',
	],
	parserOptions: {
		ecmaVersion: 12,
	},
	rules: {
		// allows output to warn or error
		'no-console': ['error', { allow: ['warn', 'error'] }],
		// allows using tabs
		'no-tabs': ['error', { allowIndentationTabs: true }],
		// indent - tab
		indent: [2, 'tab'],
		'linebreak-style': 0,
		'no-plusplus': 0,
	},
};
